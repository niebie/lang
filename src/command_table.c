#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "../include/sqlite3.h"
#include "../include/menu.h"
#include "../include/glo.h"
#include "../include/database_functions.h"

void english_to_russian_driver(void)
{
  printf("Start typing to begin lookups:\n");
  return;
  while(true)
    {
      /* TODO >
       * char last_char = getc();
       * printf("%c", last_char);
       * if(last_char == 'q'
       */
    }
}

bool search_word(const char *const word)
{
  /* TODO > Update the tri from the database? */
  /* Search the tri to see if the word requested is already there. */

  /* For now, build the tri in place from the database so that we can test it */
  /* tri_t root_tri; */
  /* char i; */
  /* root_tri.parent = NULL; */
  /* for(i = 'A'; i <= 'Z'; i++) */
  /*   { */
  /*     root_tri[TRI_INDEX(i)] = NULL; */
  /*   } */
  /* root_tri.pkey = -1; */

  return false;
}

void new_word(char *word)
{
  /* TODO > This should probably check status and such. */
  init_database();

  /* OK, we have the database open. Now we need to see if the word is
  *  already in the dictionary, right? */

  /* bool word_check =  */
}

void register_words(void)
{
  char buffer[1024];
 _L_START:
  printf("Word (nothing to return to menu):");
  fgets(buffer, 1024, stdin);
  if(buffer[0] == '\n')
    return;

  new_word(buffer);
  goto _L_START;
}

void quit_application(void)
{
  /*Do some sort of clean up???*/
  printf( "Program terminated.\n\n" );
  exit(EXIT_SUCCESS);
}

void clear_screen(void)
{
  CLEAR_SCREEN;
}

void dump_database(void)
{
  word_array_t my_array;
  printf("Dumping the database...\n");
  get_words_table(&my_array);
}

menu_item_t main_menu_items[] =
  {
    {
      "ENG->РУС",
      "etr",
      english_to_russian_driver
    },
    {
      "Register new words",
      "reg",
      register_words
    },
    {
      "Clear screen",
      "clear",
      clear_screen
    },
    {
      "Dump all words from the database.",
      "dump",
      dump_database
    },
    {
      "Exit application",
      "exit",
      quit_application
    },
    {
      0
    }
  };
