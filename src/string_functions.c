#include <ctype.h>
#include <string.h>

char * str_trim(char *str_to_trim)
{
  int len;
  char *head = str_to_trim;
  char *tail;
  while(isspace(*head))
    head++;

  len = strlen(head);
  tail = head + len - 1;

  while(isspace(*tail))
    {
      *tail = '\0';
      tail--;
    }

  return head;
}
