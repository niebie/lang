#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/sqlite3.h"
#include "../include/glo.h"
#include "../include/database_functions.h"

/* TODO > Should this be printed on the standard error output stream? */
#define CLOSE_DB(db, error_string) \
  printf(error_string);            \
  sqlite3_close(db);               \
  return;

static void init_database_from_scratch();
static void verify_database_integrity();
static void apply_migration();

void init_database()
{
  /* If the database is already there we will verify the schema. */
  /*  If the schema is incorrect we need to attempt a migration...? */
  /* Else we will create it from scratch. */
  if(access(DB_NAME, F_OK))
    {
      printf("Database file '" DB_NAME "' does not exist!\n");
      init_database_from_scratch();
      return;
    }
  if(access(DB_NAME, R_OK | W_OK)) /* returns 0 on OK. */
    {
      printf("Database file '" DB_NAME "' exists but does not have"
             "both read and write access for the current user.\n");
      return;
    }

  verify_database_integrity();
  return;
}

void insert_into_database(table_name )
     const char *const table_name;
{
/* #define INSERT_STMT( table, fields, values ) \ */
/*   "INSERT INTO " table "(" fields ") VALUES(" values ")" */

}

static void init_database_from_scratch()
{
  /* At this point I know that the file does _NOT_ exist on disk. */
  sqlite3 *database_object;
  int return_code;

  return_code = sqlite3_open(DB_NAME, &database_object);
  /* sqlite_ok == 0, so all others are error. */
  if(return_code != SQLITE_OK)
    {
      CLOSE_DB( database_object,
                "can't open db\n" );
    }

  /* We have an open connection to the database here. */
  return_code = sqlite3_exec( database_object,
                              "CREATE TABLE words "
                              "(id INTEGER PRIMARY KEY ASC,"
                              " language_id REFERENCES languages,"
                              " word TEXT)",
                              NULL,
                              NULL,
                              NULL );

  if( return_code != SQLITE_OK)
    {
      CLOSE_DB( database_object,
                "Failed to execute creation of words table.\n");
    }

  sqlite3_close(database_object);
  return;
}

static void verify_database_integrity()
{
  /* TODO > TODO > TODO ... */
  return;
  bool lSuccess = false;
  /* TODO */

  if(!lSuccess)
    {
      apply_migration();
    }


}

static void apply_migration()
{

}

int row_callback(arguments, column_count, column_data, column_names)
     void *arguments;
     int column_count;
     char **column_data;
     char **column_names;
{
  int i;
  for(i = 0; i < column_count; i++)
    {
      printf("%s:%s\n", column_names[i], column_data[i]);
    }
  return SQLITE_OK;
}

void get_words_table(word_array)
     word_array_t const *const word_array;
{
  sqlite3 *database_object;
  int return_code;

  if(word_array == NULL)
    {
      /* TODO > Need to return an error or something */
      printf("NULL WORDS ARRAY...\n");
      return;
    }

  /* TODO > Extract the following database code out into a seperate function
   *      > to share the functionality.*/
  /* For now (proof of concept) I am inlining all of the database interaction */
  if(access(DB_NAME, R_OK | W_OK)) /* returns 0 on OK. */
    {
      printf("CANT GET WORDS WITHOUT A DATABASE\n");
      return;
    }

  return_code = sqlite3_open(DB_NAME, &database_object);
  /* sqlite_ok == 0, so all others are error. */
  if(return_code != SQLITE_OK)
    {
      CLOSE_DB( database_object,
                "can't open db\n" );
    }

  /* We have an open connection to the database here. */
  return_code = sqlite3_exec( database_object,
                              "SELECT * from words",
                              row_callback,
                              NULL,
                              NULL );

  if( return_code != SQLITE_OK)
    {
      CLOSE_DB( database_object,
                "Failed to execute creation of words table.\n");
    }

  sqlite3_close(database_object);
  return;
}
