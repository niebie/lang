/* This file defines the TRI data structure and the functions that manage it. */

typedef struct lang_tri
{
  /* HOW DOES THIS WORK WITH LOCALIZATION */
  /* TODO > SHIT */
  struct lang_tri *children[26];
  struct lang_tri *parent;
  /* TODO > Do we really need this? We can traverse through index lookups and
  *       > then do a primary key lookup pretty easy if we want to know what
  *       > this exact word is. */
  /* char my_letter; */
  int pkey;
} tri_t;

#define TRI_INDEX(letter) (letter - 'A')
