#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "../include/string_functions.h"
#include "../include/menu.h"
#include "../include/prompt.h"

int menu_items_len(menu_item_t *menu_items)
{
  int count = 0;
  /* Look for a null item in the given list. */
  if(menu_items == NULL)
    {
      return count;
    }
  while(!(menu_items->display == NULL &&
          menu_items->command == NULL &&
          menu_items->cmd_call == NULL))
    {
      menu_items++;
      count++;
    }
  return count;
}

void print_menu(menu_item_t *menu_items)
{
  int count = menu_items_len(menu_items);
  int i = 0;

  for(; i < count; i++)
    {
      printf("%-5s > %s\n", menu_items[i].command, menu_items[i].display);
    }
}

static menu_item_t * find_command(menu_item_t *menu_items, char *search_command)
{
  int count = menu_items_len(menu_items);
  int i;
  for(i = 0; i < count; i++)
    {
      if(!strcmp(search_command, menu_items[i].command))
        {
          return &(menu_items[i]);
        }
    }
  return NULL;
}

void run_menu_command(menu_item_t *menu_items)
{
  char buffer[48];
  menu_item_t *selected_command;
  while(true)
    {
      char * trimmed;
      print_prompt();

      /* TODO > I need to update this so that C^p/C^n are operational */
      /* TODO > C^l should clear the screen as is expected of shells. */
      fgets( buffer, 48, stdin );

      trimmed = str_trim(buffer);
      selected_command = find_command(menu_items, trimmed);

      if(!selected_command)
        {
          printf("Invalid choice string (%s)\n", trimmed);
        }
      else
        {
          if(selected_command->cmd_call)
            {
              selected_command->cmd_call();
            }
          else
            {
              printf("Command not mapped!\n");
            }
          break;
        }
    }
}
