#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include "../include/glo.h"
#include "../include/main_menu.h"

int main(int argc, char **argv)
{
  CLEAR_SCREEN;
  printf(WELCOME_MSG);

  main_menu();

  return EXIT_SUCCESS;
}
