#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>

#include "../include/main_menu.h"
#include "../include/menu.h"
#include "../include/string_functions.h"
#include "../include/prompt.h"

#define SMALL_BUF_SIZE 48

extern menu_item_t main_menu_items[];

void main_menu()
{
  while(true)
    {
      char *lPWD = get_current_dir_name( );
      printf("..PWD:'%s'..\n", lPWD);
      free(lPWD);
      printf("..Main Menu..\n");
      print_menu(main_menu_items);
      run_menu_command(main_menu_items);
      printf("\n");
    }
}
