OUT=-o bin/lang
BIN=bin
BIN_FILES=$(BIN)/string_functions.o \
	$(BIN)/menu.o \
	$(BIN)/main_menu.o \
	$(BIN)/command_table.o \
	$(BIN)/prompt.o \
	$(BIN)/sqlite3.o \
	$(BIN)/database_functions.o

CFLAGS=-Wall -Werror -D _GNU_SOURCE -ldl -lpthread -std=c89

# NOTE > I am purposefully maintaining the source files individually so that I
#      > am always close to the build process, and so that I can make sure that
#      > this file remains as bloat-free as possible.

all:
	clear
# TODO > This should be in a clean target...
	rm -rf bin/*.o
	rm -rf bin/lang
	gcc $(CFLAGS) src/database_functions.c -c -o bin/database_functions.o
	gcc $(CFLAGS) src/sqlite3.c -c -o bin/sqlite3.o
	gcc $(CFLAGS) src/menu.c -c -o bin/menu.o
	gcc $(CFLAGS) src/string_functions.c -c -o bin/string_functions.o
	gcc $(CFLAGS) src/main_menu.c -c -o bin/main_menu.o
	gcc $(CFLAGS) src/command_table.c -c -o bin/command_table.o
	gcc $(CFLAGS) src/prompt.c -c -o bin/prompt.o
	gcc $(CFLAGS) $(BIN_FILES) src/main.c $(OUT)
	cd bin; clear; ./lang;
