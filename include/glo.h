#ifndef _GLO_H_
#define _GLO_H_
#define WELCOME_MSG "Welcome to lang, a terminal based trans-language " \
                    "dictionary!\n"

#define DB_NAME "database.sqlite"
#define CLEAR_STR "\e[1;1H\e[2J"
#define CLEAR_SCREEN printf( CLEAR_STR )
#endif
