#ifndef _MENU_H_
#define _MENU_H_

typedef struct menu_item
{
  char *display;
  char *command;
  void (*cmd_call)(void);
}menu_item_t;

/* Macros for use with menu items */
int menu_items_len(menu_item_t *menu_items);
void print_menu(menu_item_t *menu_items);
void run_menu_command(menu_item_t *menu_items);

#endif
