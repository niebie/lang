#ifndef _DATABASE_FUNCTIONS_H_
#define _DATABASE_FUNCTIONS_H_

typedef struct
{
  int pkey;
  char *word;
  int language_pkey;
} word_record_t;

typedef struct
{
  word_record_t *words;
  int count;
  int max;
} word_array_t;

void init_database();
void get_words_table(word_array_t const * const word_array);

#endif
